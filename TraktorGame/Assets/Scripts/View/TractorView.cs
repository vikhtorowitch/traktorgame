﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using TraktorGameLogic;
using UnityEngine.UI;

public class TractorView : MonoBehaviour
{
    private MapObjectTractor _mapObject;

    [SerializeField]
    private Transform _objectTransform;

    [SerializeField]
    private Image _damage;

    [SerializeField]
    private Text _tractorHealthText;

    [SerializeField]
    private Text _tractorFuelText;

    public void Init(MapObjectTractor mapObject)
    {
        _mapObject = mapObject;

        if (_mapObject != null)
        {
            _mapObject.OnMove += OnTractorMove;
            _mapObject.OnDamage += OnTractorDamage;
        }

        if (_tractorHealthText != null) _tractorHealthText.text = _mapObject.GetHealth.ToString();
        if (_tractorFuelText != null) _tractorFuelText.text = _mapObject.Fuel.ToString();
    }

    public void TurnUp(Action callback)
    {
        RotateTractor(270f, callback);
    }

    public void TurnDown(Action callback)
    {
        RotateTractor(90f, callback);
    }

    public void TurnLeft(Action callback)
    {
        RotateTractor(180f, callback);
    }

    public void TurnRight(Action callback)
    {
        RotateTractor(0f, callback);
    }

    private void OnTractorMove(int x, int y)
    {
        MoveTractor(new Vector3(x * Config.TileMultiplier, 0, y * Config.TileMultiplier));
        if (_tractorFuelText != null) _tractorFuelText.text = _mapObject.Fuel.ToString();
    }

    private void OnTractorDamage(int health)
    {
        if (_tractorHealthText != null) _tractorHealthText.text = health.ToString();

        if(_damage != null)
        {
            var color = _damage.color;
            color.a = 0.3f;
            _damage.color = color;            
        }
    }

    private float _time = 0f;
    private void FixedUpdate()
    {
        _time += Time.fixedDeltaTime;

        if (_time >= 0.1f)
        {
            if (_damage != null)
            {
                var color = _damage.color;

                if (color.a >= 0)
                {
                    color.a -= 0.05f;
                    _damage.color = color;
                }
            }

            _time = 0f;
        }
    }

    private Tweener _rotationTweener;
    private void RotateTractor(float angle, Action callback)
    {
        if (angle == _objectTransform.rotation.eulerAngles.y)
        {
            if (callback != null) callback();
            if (_mapObject.State == TractorStates.Turn) _mapObject.State = TractorStates.Idle;
            return;
        }

        if (_rotationTweener != null)
        {
            _rotationTweener.Kill();
        }

        _mapObject.State = TractorStates.Turn;
        _rotationTweener = _objectTransform.DORotate(new Vector3(0f, angle, 0f), 0.5f);
        _rotationTweener.OnComplete(() =>
        {
            _mapObject.State = TractorStates.Idle;
            if (callback != null) callback();
        });
    }

    private Tweener _moveTweener;
    private void MoveTractor(Vector3 position, Action callback = null)
    {
        _mapObject.State = TractorStates.Move;
        _moveTweener = _objectTransform.DOMove(position, 0.2f).SetEase(Ease.Linear);
        _moveTweener.OnComplete(() =>
        {
            _mapObject.State = TractorStates.Idle;
            if (callback != null) callback();
        });
    }
}
