﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverView : MonoBehaviour
{
    [SerializeField]
    private GameObject _winText;
    [SerializeField]
    private GameObject _looseText;
    [SerializeField]
    private Button _button;

    void Start ()
    {
        _button.onClick.RemoveAllListeners();
        _button.onClick.AddListener(() => { SceneManager.LoadScene("Start"); });
	}

    public void ShowWin()
    {
        gameObject.SetActive(true);
        _winText.SetActive(true);
        _looseText.SetActive(false);
    }

    public void ShowLoose()
    {
        gameObject.SetActive(true);
        _winText.SetActive(false);
        _looseText.SetActive(true);
    }

}
