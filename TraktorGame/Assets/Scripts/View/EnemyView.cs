﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TraktorGameLogic;
using DG.Tweening;
using UnityEngine.UI;

public class EnemyView : MonoBehaviour
{
    private MapObject _mapObject;

    [SerializeField]
    private GameObject _normalBody;
    [SerializeField]
    private GameObject _deadBody;
    [SerializeField]
    private Text _enemyCountText;

    private GameLogic _gameLogic;

    private Vector2 _savedCoords;

    public void Init(GameLogic gameLogic, MapObject mapObject, Text enemyCountText)
    {
        _gameLogic = gameLogic;
        _mapObject = mapObject;
        _enemyCountText = enemyCountText;
        
		if (_mapObject != null)
        {
            _mapObject.OnMove += ChangePosition;
            
            if (_mapObject is MapObjectRoach)
            {
                (_mapObject as MapObjectRoach).OnStateChanged += StateChanged;
            }

            _savedCoords = new Vector2(_mapObject.MapX, _mapObject.MapY);
        }
	}

    private void ChangePosition(int newX, int newY)
    {
        var newCoords = new Vector2(newX, newY);
        var direction = newCoords - _savedCoords;

        var angle = Vector2.Angle(Vector2.up, direction.normalized) * (direction.x < 0 ? -1 : 1);

        transform.rotation = Quaternion.Euler(0, angle, 0);

        transform.DOMove(new Vector3(newX * Config.TileMultiplier, 0, newY * Config.TileMultiplier), 0.5f);

        _savedCoords = newCoords;
    }

    private void StateChanged(RoachStates state)
    {
        if (state == RoachStates.Dead)
        {
            _deadBody.SetActive(true);
            _normalBody.SetActive(false);

            _enemyCountText.text = _gameLogic.GetLiveEnemiesCount().ToString();
        }
    }
}
