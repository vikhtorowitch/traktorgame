﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace TraktorGameLogic
{
    public enum MapObjectTypes
    {
        Tractor,
        Roach,
        Other
    }

    public enum RoachStates
    {
        Seek,
        Runout,
        Attack,
        Rest,
        Dead
    }

    public enum TractorStates
    {
        Idle,
        Turn,
        Move
    }

    public interface IHealthHandler
    {
        bool IsAlive();
        void SetDamage(int damage);
    }

    public interface ITicker
    {
        void Tick();
    }

    public interface IWeapon
    {
        void Attack(IHealthHandler target);
    }

    public abstract class MapObject
    {
        public int MapX { get { return _mapX; } }
        public int MapY { get { return _mapY; } }

        public event Action<int, int> OnMove;

        protected int _mapX { get; set; }
        protected int _mapY { get; set; }

        public MapObject(int x, int y)
        {
            SetXY(x, y);
        }

        public void SetXY(int x, int y)
        {
            if (OnMove != null) OnMove(x, y);

            _mapX = x;
            _mapY = y;

            AfterMove(x, y);
        }

        public void SetXY(Vector2 mapCellCoords)
        {
            SetXY((int)mapCellCoords.x, (int)mapCellCoords.y);
        }

        protected virtual void AfterMove(int x, int y) {}
    }

    public abstract class LiveMapObject : MapObject, IHealthHandler
    {
        private int _health;
        public int GetHealth { get { return _health; } } 

        public event Action<int> OnDamage;

        public LiveMapObject(int x, int y, int health) : base(x, y)
        {
            _health = health;
        }

        public bool IsAlive()
        {
            return (_health > 0);
        }

        public void SetDamage(int damage)
        {
            _health -= damage;

            if (_health < 0) _health = 0;

            if (OnDamage != null) OnDamage(_health);
        }
    }

    public class MapObjectTractor : LiveMapObject
    {
        public TractorStates State { get; set; }

        public int Fuel { get { return _fuel; } }
        private int _fuel;

        public MapObjectTractor(int x, int y, int health, int fuel) : base(x, y, health)
        {
            _fuel = fuel;
            State = TractorStates.Idle;
        }

        protected override void AfterMove(int x, int y)
        {
            _fuel--;

            if (_fuel < 0) _fuel = 0;
        }
    }

    public class RoachBarb : IWeapon
    {
        public void Attack(IHealthHandler target)
        {
            if (target != null) target.SetDamage(Config.DefaultBarbAttackPower);
        }
    }

    public class MapObjectRoach : LiveMapObject, ITicker
    {
        public event Action<RoachStates> OnStateChanged;

        private RoachStates _state;
        private int _tikker;

        private GameLogic _gameLogic;
        
        private IWeapon _weapon;

        private int _defaultStamina;
        private int _stamina;

        public MapObjectRoach(int x, int y, int health, int stamina, IWeapon weapon, GameLogic gameLogic) : base(x, y, health)
        {
            _stamina = stamina;
            _defaultStamina = stamina;
            _weapon = weapon;
            _gameLogic = gameLogic;
            SetState(RoachStates.Seek);
            _tikker = 0;
        }

        public void Tick()
        {
            _tikker++;

            var distanceToTractor = 0f;

            if (_state != RoachStates.Dead)
            {
                distanceToTractor = GetDistanceToTractor();

                if ((int)distanceToTractor == 0)
                {
                    SetDamage(Config.DefaultEnemyHealth);
                    SetState(RoachStates.Dead);
                    return;
                }
            }
            else
            {
                return;
            }

            switch(_state)
            {
                case RoachStates.Attack:
                    if (_tikker >= Config.EnemyAttackRate)
                    {
                        if (distanceToTractor == 1 && _stamina > 0)
                        {
                            Attack(_gameLogic.Player);                            
                        }
                        else
                        {
                            SetState(RoachStates.Seek);
                            return;
                        }

                        _tikker = 0;
                    }

                    break;
                case RoachStates.Seek:
                    if (_tikker >= Config.EnemySeekRate)
                    {
                        if (distanceToTractor <= 3 && _gameLogic.GetTractorState() != TractorStates.Idle)
                        {
                            SetState(RoachStates.Runout);                            
                            return;
                        }
                        
                        if (distanceToTractor == 1)
                        {
                            SetState(RoachStates.Attack);                            
                            return;
                        }

                        Seek();
                        _tikker = 0;
                    }

                    break;
                case RoachStates.Runout:
                    if (_tikker >= Config.EnemyRunoutRate)
                    {
                        if (distanceToTractor > 5)
                        {
                            SetState(RoachStates.Seek);
                            return;
                        }

                        Runout();
                        _tikker = 0;
                    }
                    break;
                case RoachStates.Rest:
                    if (_tikker >= Config.DefaultEnemyStaminaCooldown)
                    {
                        _stamina = _defaultStamina;
                        SetState(RoachStates.Seek);
                    }
                    break;
            }
        }

        private void SetState(RoachStates state)
        {
            _state = state;
            _tikker = 0;

            if (OnStateChanged != null) OnStateChanged(_state);
        }

        private float GetDistanceToTractor()
        {
            return _gameLogic.GetDistanceToTractorFrom(_mapX, _mapY);
        }

        private bool CanMoveRoach(int x, int y)
        {
            var other = _gameLogic.GetEnemyFrom(x, y);
            if (other != null)
            {
                if (!(other as IHealthHandler).IsAlive())
                {
                    other = null;
                }
            }

            return (_gameLogic.CanMoveTo(x, y) && other == null);
        }

        private bool CanMoveRoach(Vector2 mapCellCoords)
        {
            return CanMoveRoach((int)mapCellCoords.x, (int)mapCellCoords.y);
        }

        private void Attack(IHealthHandler target)
        {
            if (target == null) return;
            if (target.IsAlive()) _weapon.Attack(target);
        }

        private void UseStamina()
        {
            _stamina--;

            if (_stamina <= 0)
            {
                _stamina = 0;
                SetState(RoachStates.Rest);
            }
        }

        private void Seek()
        {
            int moveX = Random.Range(-1, 2);
            int moveY = Random.Range(-1, 2);

            int rnd = Random.Range(1, 11);
            bool directionX = (rnd > 5);

            if (directionX)
            {
                moveY = 0;
            }
            else
            {
                moveX = 0;
            }

            var newX = _mapX + moveX;
            var newY = _mapY + moveY;

            if (CanMoveRoach(newX, newY))
            {                
                SetXY(newX, newY);
                UseStamina();
            }
        }

        private void Runout()
        {
            var sides = new Vector2[4];
            sides[0] = new Vector2(_mapX - 1, _mapY);
            sides[1] = new Vector2(_mapX, _mapY - 1);
            sides[2] = new Vector2(_mapX + 1, _mapY);
            sides[3] = new Vector2(_mapX, _mapY + 1);

            var maxDistance = 0f;
            Vector2 maxCoords = Vector2.zero;

            foreach(var side in sides)
            {
                var dist = _gameLogic.GetDistanceToTractorFrom(side);
                if (dist > maxDistance && CanMoveRoach(side))
                {
                    maxDistance = dist;
                    maxCoords = side;
                }
            }

            if (maxDistance > 0)
            {
                SetXY(maxCoords);
                UseStamina();
            }
        }
    }

}