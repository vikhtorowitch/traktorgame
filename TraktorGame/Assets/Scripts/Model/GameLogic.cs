﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace TraktorGameLogic
{
    public enum GameStates
    {
        Init,
        Running,
        Loose,
        Win
    }

    public class GameLogic
    {
        public GameStates GameState { get { return _gameState; } }

        public int MapWidth { get { return _mapWidth; } }
        public int MapHeight { get { return _mapHeight; } }

        public MapTile[,] Map { get { return _map; } }

        public MapObjectTractor Player { get { return _player; } }

        public List<MapObject> Enemies { get { return _enemies; } }

        public event Action<GameStates> OnStateChanged;
        public event Action<MapObject> OnSpawnRoach;

        private GameStates _gameState;

        private int _mapWidth;
        private int _mapHeight;

        private int _startEnemiesCount;
        private int _totalEnemiesCount;

        private MapTile[,] _map;

        private MapObjectTractor _player;

        private List<MapObject> _enemies;

        private List<Vector2> _holes;

        public GameLogic(GameMap map, Action<GameLogic> onInitComplete = null)
        {
            SetState(GameStates.Init);

            InitMap(map);
            InitPlayer();
            InitEnemies(map.StartEnemiesCount);

            _player.OnMove += OnTractorMove;

            if (onInitComplete != null) onInitComplete(this);

            SetState(GameStates.Running);
        }

        private int _tikker = 0;
        private int _spawnTikker = 0;

        public void Tick()
        {
            if (_gameState != GameStates.Running) return;

            if (_enemies != null)
            {
                foreach(var enemy in _enemies)
                {
                    if (enemy is ITicker)
                    {
                        ((ITicker)enemy).Tick();
                    }
                }
            }

            if (_tikker >= 20)
            {
                if (CheckWinCondition()) SetState(GameStates.Win);
                if (CheckLooseCondition()) SetState(GameStates.Loose);
                _tikker = 0;
            }

            if (_spawnTikker >= 1000)
            {
                if (_enemies.Count < _totalEnemiesCount)
                {
                    var spawnCount = Random.Range(1, _totalEnemiesCount - _enemies.Count);

                    for (var i = 0; i < spawnCount; i++)
                    {
                        SpawnRoach();
                    }
                }

                _spawnTikker = 0;
            }

            _tikker++;
            _spawnTikker++;
        }

        public bool CanMoveTo(int x, int y)
        {
            if (_map == null) return false;

            if (x >= _map.GetLength(0) || x < 0) return false;
            if (y >= _map.GetLength(1) || y < 0) return false;

            if (!_map[x, y].CanMove) return false;

            return true;
        }

        public MapObject GetEnemyFrom(int x, int y)
        {
            foreach (var enemy in _enemies)
            {
                if (enemy.MapX == x && enemy.MapY == y)
                {
                    return enemy;
                }
            }

            return null;
        }

        public MapObject GetEnemyFrom(Vector2 mapCellCoords)
        {
            return GetEnemyFrom((int)mapCellCoords.x, (int)mapCellCoords.y);
        }

        public bool CanMoveTo(Vector2 mapCellCoords)
        {
            return CanMoveTo((int)mapCellCoords.x, (int)mapCellCoords.y);
        }

        public float GetDistanceToTractorFrom(int x, int y)
        {
            return Vector2.Distance(new Vector2(Player.MapX, Player.MapY), new Vector2(x, y));
        }

        public float GetDistanceToTractorFrom(Vector2 mapCellCoords)
        {
            return Vector2.Distance(new Vector2(Player.MapX, Player.MapY), mapCellCoords);
        }

        public TractorStates GetTractorState()
        {
            return (Player as MapObjectTractor).State;
        }

        public int GetTractorHealth()
        {
            return (Player as MapObjectTractor).GetHealth;
        }

        public int GetTractorFuel()
        {
            return (Player as MapObjectTractor).Fuel;
        }

        public int GetLiveEnemiesCount()
        {
            return _totalEnemiesCount - _enemies.Where(i => !(i as LiveMapObject).IsAlive()).Count();
        }        

        private void InitMap(GameMap map)
        {
            _map = new MapTile[map.MapWidth, map.MapHeight];

            _mapWidth = map.MapWidth;
            _mapHeight = map.MapHeight;

            _startEnemiesCount = map.StartEnemiesCount;
            _totalEnemiesCount = map.TotalEnemiesCount;

            _holes = new List<Vector2>();

            for (var y = 0; y < _mapHeight; y++)
            {
                for (var x = 0; x < _mapWidth; x++)
                {
                    var tileType = (MapTileTypes)map.MapArray[x, y];
                    _map[x, y] = MapTile.Create(tileType);

                    if (tileType == MapTileTypes.Hole)
                    {
                        _holes.Add(new Vector2(x, y));
                    }
                }
            }
        }

        private Vector2 GetFreeRandomPosition()
        {
            var rndX = Random.Range(0, _mapWidth);
            var rndY = Random.Range(0, _mapHeight);

            var counter = 0;

            while (!CanMoveTo(rndX, rndY) && counter < 100)
            {
                rndX = Random.Range(0, _mapWidth);
                rndY = Random.Range(0, _mapHeight);
                counter++;
            }

            if (counter >= 100)
            {
                for (var y = 0; y < _mapHeight; y++)
                {
                    for (var x = 0; x < _mapWidth; x++)
                    {
                        if (CanMoveTo(x, y))
                        {
                            return new Vector2(x, y);
                        }
                    }
                }
            }
            else
            {
                return new Vector2(rndX, rndY);
            }

            return new Vector2(-1, -1);
        }

        private void InitPlayer()
        {
            var playerPosition = GetFreeRandomPosition();

            if (playerPosition.x >= 0)
            {
                _player = new MapObjectTractor((int)playerPosition.x, (int)playerPosition.y, Config.DefaultPlayerHealth, Config.DefaultTractorFuel);
            }
            else
            {
                Debug.LogError("No free space on map for player!");
            }
        }

        private void InitEnemies(int enemiesCount)
        {
            _enemies = new List<MapObject>();

            for (var i = 0; i < enemiesCount; i++)
            {
                var enemyPosition = GetFreeRandomPosition();
                if (enemyPosition.x >= 0)
                {
                    _enemies.Add(new MapObjectRoach((int)enemyPosition.x, (int)enemyPosition.y, Config.DefaultEnemyHealth, Config.DefaultEnemyStamina, new RoachBarb(), this));
                }
            }
        }

        private void SpawnRoach()
        {
            var rnd = Random.Range(0, _holes.Count);
            var spawnHole = _holes[rnd];

            var newRoach = new MapObjectRoach((int)spawnHole.x, (int)spawnHole.y, Config.DefaultEnemyHealth, Config.DefaultEnemyStamina, new RoachBarb(), this);
            _enemies.Add(newRoach);

            if (OnSpawnRoach != null)
            {
                OnSpawnRoach(newRoach);
            }
        }

        private void OnTractorMove(int x, int y)
        {
            if (_map[x, y] is IDamager)
            {
                _player.SetDamage((_map[x, y] as IDamager).GetDamage());
            }
        }

        private void SetState(GameStates state)
        {
            _gameState = state;
            if (OnStateChanged != null) OnStateChanged(_gameState);
        }

        private bool CheckWinCondition()
        {
            return GetLiveEnemiesCount() == 0;
        }

        private bool CheckLooseCondition()
        {
            return (GetTractorHealth() == 0) || (GetTractorFuel() == 0);
        }
    }
}
