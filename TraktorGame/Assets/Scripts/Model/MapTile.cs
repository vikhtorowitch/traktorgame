﻿namespace TraktorGameLogic
{
    public enum MapTileTypes
    {
        None,
        Grass,
        Rock,
        Hole,
        Mine,
        Tree,
    }

    public interface IDamager
    {
        int GetDamage();
    }

    public abstract class MapTile
    {
        public MapTileTypes Type { get; protected set; }
        public bool CanMove { get; protected set; }

        public MapTile(MapTileTypes type, bool canMove)
        {
            Type = type;
            CanMove = canMove;
        }

        public static MapTile Create(MapTileTypes tileType)
        {
            switch (tileType)
            {
                case MapTileTypes.Grass: return new MapTileGrass(); break;
                case MapTileTypes.Rock: return new MapTileRock(); break;
                case MapTileTypes.Mine: return new MapTileMine(); break;
                case MapTileTypes.Hole: return new MapTileHole(); break;
                case MapTileTypes.Tree: return new MapTileTree(); break;
            }

            return null;
        }
    }

    public class MapTileGrass : MapTile
    {
        public MapTileGrass() : base(MapTileTypes.Grass, true) { }
    }

    public class MapTileRock : MapTile
    {
        public MapTileRock() : base(MapTileTypes.Rock, false) { }
    }

    public class MapTileTree : MapTile
    {
        public MapTileTree() : base(MapTileTypes.Tree, false) { }
    }

    public class MapTileHole : MapTile
    {
        public MapTileHole() : base(MapTileTypes.Hole, false) { }
    }

    public class MapTileMine : MapTile, IDamager
    {
        public MapTileMine() : base(MapTileTypes.Mine, true) { }

        public int GetDamage()
        {
            return Config.DefaultMineDamage;
        }
    }
}