﻿namespace TraktorGameLogic
{
    public static class Config
    {
        public static int DefaultMineDamage = 1;
        public static int DefaultPlayerHealth = 10;
        public static int DefaultTractorFuel = 500;
        public static int DefaultEnemyHealth = 1;
        public static int DefaultBarbAttackPower = 1;

        public static int DefaultEnemyStamina = 40;
        public static int DefaultEnemyStaminaCooldown = 40;

        public static int EnemyAttackRate = 40;
        public static int EnemySeekRate = 20;
        public static int EnemyRunoutRate = 12;

        public static int TileMultiplier = 10;

        public static string LevelsServerURL = "http://tg.aliex.ru/levels.php";
    }

    public class GameMapHeader
    {
        public string MapName { get; set; }
        public int MapWidth { get; set; }
        public int MapHeight { get; set; }
        public int StartEnemiesCount { get; set; }
        public int TotalEnemiesCount { get; set; }
    }

    public class GameMap : GameMapHeader
    {    
        public byte[,] MapArray { get; set; }
    }

    public class WebLevelHeader
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
