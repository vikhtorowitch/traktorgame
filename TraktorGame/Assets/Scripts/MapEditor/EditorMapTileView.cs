﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditorMapTileView : MonoBehaviour
{
    public TileView Tile { get { return _tile; } }
    private TileView _tile;

    public void SetTile(TileView tilePrefab)
    {
        if (_tile != null)
        {
            DestroyImmediate(_tile.gameObject);
        }

        _tile = Instantiate(tilePrefab);
        _tile.transform.SetParent(transform);
        _tile.transform.localPosition = Vector3.zero;
        /*
        var collider = gameObject.AddComponent<MeshCollider>();
        collider.sharedMesh = _tile.GetComponent<MeshFilter>().sharedMesh;
        collider.convex = true;
        */
    }    
}
