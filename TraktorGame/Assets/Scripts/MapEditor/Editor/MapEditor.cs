﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using TraktorGameLogic;
using Newtonsoft.Json;
using System.IO;
using UnityEditor.SceneManagement;
using System.Text;

public class MapEditor : EditorWindow
{
    [MenuItem("Tools/Map Editor")]
    public static void Init()
    {
        MapEditor window = (MapEditor)GetWindow(typeof(MapEditor));
        window.titleContent.text = "Map editor";

        EditorSceneManager.OpenScene("Assets/Scenes/MapEdit.unity");
        var mapEditGO = GameObject.Find("EditMap");
        if (mapEditGO != null)
        {
            _mapContainer = mapEditGO;            
        }

        window.Show();
    }

    private const string _prefabsPath = "Prefabs/Tiles";

    private static GameObject _mapContainer;
    private int _enemiesStartCount = 30;
    private int _enemiesTotalCount = 100;

    private int _mapWidth = 50;
    private int _mapHeight = 50;

    private string _mapName = "Level 1";

    private string _savePath = "/Resources/Levels/Level1.txt";

    private MapTileTypes _selectTileType;

    private EditorMapTileView[,] _tiles;

    private Dictionary<MapTileTypes, TileView> _tilePrefabs;

    private List<WebLevelHeader> _webLevels = null;
    private List<string> _webLevelsNames = null;

    private int _selectedWebLevel = -1;

    private void OnGUI()
    {
        GUILayout.Space(20);
        _mapContainer = (GameObject)EditorGUILayout.ObjectField("Map container: ", _mapContainer, typeof(GameObject), true);
        GUILayout.Space(20);

        _mapName = EditorGUILayout.TextField("Map name: ", _mapName);
        GUILayout.Space(20);

        _enemiesStartCount = EditorGUILayout.IntField("Count of roaches on start: ", _enemiesStartCount);
        GUILayout.Space(10);
        _enemiesTotalCount = EditorGUILayout.IntField("Total count of roaches: ", _enemiesTotalCount);
        GUILayout.Space(20);

        _mapWidth = EditorGUILayout.IntField("Map width: ", _mapWidth);
        GUILayout.Space(10);
        _mapHeight = EditorGUILayout.IntField("Map height: ", _mapHeight);
        GUILayout.Space(20);

        if (GUILayout.Button("Init map"))
        {
            InitMap();
        }

        GUILayout.Space(20);

        GUILayout.Label("Select tile type");

        if (_tilePrefabs == null) InitDict();

        var tiles = System.Enum.GetNames(typeof(MapTileTypes));

        _selectTileType = (MapTileTypes)EditorGUILayout.Popup(string.Empty, (int)_selectTileType, tiles);
        
        if (GUILayout.Button("Set tiles"))
        {            
            foreach (var obj in Selection.gameObjects)
            {
                var tile = obj.GetComponentInParent<EditorMapTileView>();
                if (tile != null)
                {
                    tile.SetTile(_tilePrefabs[_selectTileType]);
                }
            }
        }

        GUILayout.Space(20);

        _savePath = EditorGUILayout.TextField("Map filename: ", _savePath);

        GUILayout.Space(20);

        if (GUILayout.Button("Load map"))
        {
            LoadMap();
        }

        GUILayout.Space(10);

        if (GUILayout.Button("Save map"))
        {
            SaveMap();
        }

        GUILayout.Space(30);
        GUILayout.Label("Levels on server");
        GUILayout.Space(20);

        if (_webLevelsNames == null) LoadWebLevels();

        _selectedWebLevel = EditorGUILayout.Popup(string.Empty, _selectedWebLevel, _webLevelsNames.ToArray());

        if (GUILayout.Button("Add new from editor"))
        {
            AddToWeb();
        }

        if (GUILayout.Button("Load selected to editor"))
        {
            LoadFromWeb();
        }

        GUILayout.Space(10);

        if (GUILayout.Button("Update selected from editor"))
        {
            UpdateOnWeb();
        }

        if (GUILayout.Button("Remove selected from server"))
        {
            RemoveFromWeb();
        }

    }

    private void InitDict()
    {
        var tiles = Resources.LoadAll<TileView>(_prefabsPath);

        _tilePrefabs = new Dictionary<MapTileTypes, TileView>();

        foreach(var tile in tiles)
        {
            _tilePrefabs[tile.TileType] = tile;
        }
    }

    private void LoadMap()
    {
        LoadMap(File.ReadAllText(Application.dataPath + _savePath));        
    }

    private void LoadMap(string jsonText)
    {
        var reader = new StringReader(jsonText);
        var json = JsonSerializer.Create();
        GameMap map = (GameMap)json.Deserialize(reader, typeof(GameMap));

        InitMap(map);
    }

    private void InitMap(GameMap map = null)
    {
        InitDict();

        if (map != null)
        {
            _mapName = map.MapName;
            _mapWidth = map.MapWidth;
            _mapHeight = map.MapHeight;

            _enemiesStartCount = map.StartEnemiesCount;
            _enemiesTotalCount = map.TotalEnemiesCount;
        }

        if (_mapContainer != null)
        {
            var tiles = _mapContainer.GetComponentsInChildren<EditorMapTileView>();
            if (tiles != null)
            {
                for (var i = 0; i < tiles.Length; i++)
                {
                    GameObject.DestroyImmediate(tiles[i].gameObject);
                }
            }
        }

        _tiles = new EditorMapTileView[_mapWidth, _mapHeight];

        for (var y = 0; y < _mapHeight; y++)
        {
            for (var x = 0; x < _mapWidth; x++)
            {
                var go = new GameObject("EditorTile");
                
                var newTile = _tiles[x, y] = go.AddComponent<EditorMapTileView>();                
                newTile.transform.SetParent(_mapContainer.transform);

                if (map == null)
                {
                    newTile.SetTile(_tilePrefabs[MapTileTypes.Grass]);
                } else
                {
                    newTile.SetTile(_tilePrefabs[(MapTileTypes)map.MapArray[x, y]]);
                }

                newTile.transform.localPosition = new Vector3(x * Config.TileMultiplier, 0, y * Config.TileMultiplier);
            }
        }
    }

    private void SaveMap()
    {
        File.WriteAllText(Application.dataPath + _savePath, GetMapJSON());
    }

    private string GetMapJSON()
    {
        var map = new GameMap();
        map.MapName = _mapName;
        map.MapWidth = _mapWidth;
        map.MapHeight = _mapHeight;
        map.StartEnemiesCount = _enemiesStartCount;
        map.TotalEnemiesCount = _enemiesTotalCount;

        map.MapArray = new byte[_mapWidth, _mapHeight];

        for (var y = 0; y < _mapHeight; y++)
        {
            for (var x = 0; x < _mapWidth; x++)
            {
                map.MapArray[x, y] = (byte)_tiles[x, y].Tile.TileType;
            }
        }

        var writer = new StringWriter();
        var json = JsonSerializer.Create();

        json.Serialize(writer, map);

        return writer.ToString();
    }

    private void LoadWebLevels()
    {
        _webLevelsNames = new List<string>();
        _webLevels = new List<WebLevelHeader>();

        var result = NetworkHelper.GetRequestSync(Config.LevelsServerURL + "?action=info");

        if (string.IsNullOrEmpty(result)) return;        

        var json = new JSONObject(result);               

        if (json["levels"])
        {
            foreach (var level in json["levels"].list)
            {
                _webLevelsNames.Add(level["name"].str);
                _webLevels.Add(new WebLevelHeader() { id = (int)level["id"].n, name = level["name"].str });
            }
        }
    }

    private void LoadFromWeb()
    {
        var id = _webLevels[_selectedWebLevel].id;

        var result = NetworkHelper.GetRequestSync(Config.LevelsServerURL + "?action=info&id=" + id.ToString());

        LoadMap(result);
    }

    private void UpdateOnWeb()
    {
        if (_webLevels.Count == 0) return;

        var mapJSON = GetMapJSON();
        var id = _webLevels[_selectedWebLevel].id;

        var form = new WWWForm();
        form.AddField("action", "update");
        form.AddField("id", id);
        form.AddField("name", _mapName);
        form.AddField("json", mapJSON);

        NetworkHelper.PostRequestSync(Config.LevelsServerURL, form);

        LoadWebLevels();
    }

    private void AddToWeb()
    {
        if (_webLevels.Count == 0) return;

        var mapJSON = GetMapJSON();

        var form = new WWWForm();
        form.AddField("action", "add");
        form.AddField("name", _mapName);
        form.AddField("json", mapJSON);

        NetworkHelper.PostRequestSync(Config.LevelsServerURL, form);

        LoadWebLevels();
    }

    private void RemoveFromWeb()
    {
        if (_webLevels.Count == 0) return;

        var id = _webLevels[_selectedWebLevel].id;

        var form = new WWWForm();
        form.AddField("action", "delete");
        form.AddField("id", id);

        NetworkHelper.PostRequestSync(Config.LevelsServerURL, form);

        LoadWebLevels();

        if (_webLevels.Count > 0)
        {
            _selectedWebLevel = 0;
        }
        else
        {
            _selectedWebLevel = -1;
        }
    }
}
