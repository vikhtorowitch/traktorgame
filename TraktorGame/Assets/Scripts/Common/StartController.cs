﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TraktorGameLogic;
using System.IO;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.Events;

public class StartController : MonoBehaviour
{
    [SerializeField]
    private Transform _content;

    [SerializeField]
    private Button _buttonPrefab;

    [SerializeField]
    private SelectedLevel _selectedLevel;

    void Start ()
    {
        LoadFromWeb();
	}

    private bool LoadFromWeb()
    {
        var result = NetworkHelper.GetRequestSync(Config.LevelsServerURL + "?action=info");

        if (string.IsNullOrEmpty(result)) return false;

        var json = new JSONObject(result);

        if (json["levels"])
        {
            foreach (var level in json["levels"].list)
            {
                MakeLevelButton(level["name"].str, () =>
                {
                    var levelJson = NetworkHelper.GetRequestSync(Config.LevelsServerURL + "?action=info&id=" + ((int)level["id"].n).ToString());

                    if (!string.IsNullOrEmpty(levelJson))
                    {
                        _selectedLevel.Level = new StringReader(levelJson);
                        SceneManager.LoadScene("Main");
                    }
                });
            }
        }

        return true;
    }

    private void LoadFromResources()
    {
        var levels = Resources.LoadAll("Levels/");

        foreach (var level in levels)
        {
            var text = (level as TextAsset).text;
            var reader = new StringReader(text);
            var json = Newtonsoft.Json.JsonSerializer.Create();
            GameMapHeader mapHeader = (GameMapHeader)json.Deserialize(reader, typeof(GameMapHeader));

            MakeLevelButton(mapHeader.MapName, () =>
            {
                _selectedLevel.Level = new StringReader((level as TextAsset).text);
                SceneManager.LoadScene("Main");
            });
        }
    }

    private void MakeLevelButton(string name, UnityAction onClick)
    {
        var button = Instantiate(_buttonPrefab);
        button.gameObject.transform.SetParent(_content);

        button.gameObject.transform.FindChild("Text").GetComponent<Text>().text = name;

        button.onClick.AddListener(onClick);
    }
}
