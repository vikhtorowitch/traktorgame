﻿using System.Collections;
using System.Collections.Generic;
using CnControls;
using UnityEngine;
using UnityEngine.UI;
using TraktorGameLogic;
using DG.Tweening;
using System.IO;
using Newtonsoft.Json;
using System.Text;

public class TilePrefabLink
{
    public MapTileTypes TileType { get; set; }
    public GameObject Prefab { get; set; }
}

public class GameController : MonoBehaviour
{
    private const string _prefabsPath = "Prefabs/Tiles";

    public Text InputDebugText;

    [SerializeField]
    private TractorView _tractorView;

    [SerializeField]
    private GameOverView _gameOverView;

    [SerializeField]
    private Text _enemyCountText;

    [SerializeField]
    private GameObject _cameraContainer;
    [SerializeField]
    private GameObject _mapContainer;
    [SerializeField]
    private GameObject _enemiesContainer;
   
    private Dictionary<MapTileTypes, TileView> _tilePrefabs;

    [SerializeField]
    private EnemyView _enemyPrefab;

    [SerializeField]
    private GameObject _tractor;

    private GameLogic _gameLogic;
	
	void Start ()
    {
        var levelAsset = FindObjectOfType<SelectedLevel>();
        var map = LoadLevel(levelAsset.Level);
        _gameLogic = new GameLogic(map);

        InitMap();
        InitPlayer();
        InitEnemies();

        _gameLogic.OnSpawnRoach += SpawnEnemy;
        _gameLogic.OnStateChanged += GameStateChanged;

        Destroy(levelAsset.gameObject);
    }

    private void FixedUpdate()
    {
        if (_gameLogic != null)
        {
            _gameLogic.Tick();
        }
    }

    private void Update()
    {
        var movementVector = new Vector3(CnInputManager.GetAxis("Horizontal"), 0f, CnInputManager.GetAxis("Vertical"));
        var tractorState = (_gameLogic.Player as MapObjectTractor).State;

               
        if (tractorState == TractorStates.Idle) // || _tractorController.TractorState == TractorStates.Rotating)
        {
            if (movementVector.x > 0) _tractorView.TurnRight(() => AfterTractorRotation(movementVector));
            if (movementVector.x < 0) _tractorView.TurnLeft(() => AfterTractorRotation(movementVector));

            if (movementVector.z > 0) _tractorView.TurnUp(() => AfterTractorRotation(movementVector));
            if (movementVector.z < 0) _tractorView.TurnDown(() => AfterTractorRotation(movementVector));
        }
    }

    private void AfterTractorRotation(Vector3 movementVector)
    {
        var newX = _gameLogic.Player.MapX + (int)(-movementVector.z);
        var newY = _gameLogic.Player.MapY + (int)(movementVector.x);

        if (_gameLogic.CanMoveTo(newX, newY))
        {
            _gameLogic.Player.SetXY(newX, newY);
            _cameraContainer.transform.DOMove(new Vector3(newX * Config.TileMultiplier, 0, newY * Config.TileMultiplier), 0.2f).SetEase(Ease.Linear);           
        }
    }
    
    private GameMap LoadLevel(StringReader level)
    {        
        var json = JsonSerializer.Create();
        return (GameMap)json.Deserialize(level, typeof(GameMap));
    }

    private void InitDict()
    {
        var tiles = Resources.LoadAll<TileView>(_prefabsPath);

        _tilePrefabs = new Dictionary<MapTileTypes, TileView>();

        foreach (var tile in tiles)
        {
            _tilePrefabs[tile.TileType] = tile;
        }
    }

    private void InitMap()
    {
        InitDict();

        if (_mapContainer != null)
        {
            var tiles = _mapContainer.GetComponentsInChildren<TileView>();
            if (tiles != null)
            {
                for (var i = 0; i < tiles.Length; i++)
                {
                    GameObject.Destroy(tiles[i].gameObject);
                }
            }
        }

        for (var y = 0; y < _gameLogic.MapHeight; y++)
        {
            for (var x = 0; x < _gameLogic.MapWidth; x++)
            {
                var tile = Instantiate(_tilePrefabs[_gameLogic.Map[x, y].Type]);
                tile.transform.SetParent(_mapContainer.transform);

                tile.transform.localPosition = new Vector3(x * Config.TileMultiplier, 0, y * Config.TileMultiplier);
            }
        }
    }

    private void InitPlayer()
    {
        _tractorView.Init((MapObjectTractor)_gameLogic.Player);

        _tractor.transform.position = new Vector3(_gameLogic.Player.MapX * Config.TileMultiplier, 0, _gameLogic.Player.MapY * Config.TileMultiplier);
        _cameraContainer.transform.position = _tractor.transform.position;
    }

    private void InitEnemies()
    {
        if (_enemiesContainer != null)
        {
            var enemies = _enemiesContainer.GetComponentsInChildren<EnemyView>();
            if (enemies != null)
            {
                for (var i = 0; i < enemies.Length; i++)
                {
                    GameObject.Destroy(enemies[i].gameObject);
                }
            }
        }

        foreach (var enemy in _gameLogic.Enemies)
        {
            SpawnEnemy(enemy);
        }

        _enemyCountText.text = _gameLogic.GetLiveEnemiesCount().ToString();
    }

    private void SpawnEnemy(MapObject enemy)
    {
        var enemyPrefab = Instantiate(_enemyPrefab);
        enemyPrefab.Init(_gameLogic, enemy, _enemyCountText);

        enemyPrefab.transform.SetParent(_enemiesContainer.transform);

        enemyPrefab.transform.localPosition = new Vector3(enemy.MapX * Config.TileMultiplier, 0, enemy.MapY * Config.TileMultiplier);
    }

    private void GameStateChanged(GameStates state)
    {
        switch (state)
        {
            case GameStates.Win: _gameOverView.ShowWin(); break;
            case GameStates.Loose: _gameOverView.ShowLoose(); break;
        }
    }
}
