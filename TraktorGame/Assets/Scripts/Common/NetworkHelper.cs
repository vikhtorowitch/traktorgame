﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.Networking;

public static class NetworkHelper
{
    public static string GetRequestSync(string url)
    {
        using (UnityWebRequest www = UnityWebRequest.Get(url))
        {
            var request = www.Send();

            while (!request.isDone) Thread.Sleep(5);

            if (www.isError)
            {
                Debug.Log("GET Error: " + url);
                return null;
            }
            else
            {
                Debug.Log("GET Succesful: " + url);
                return www.downloadHandler.text;
            }
        }
    }

    public static string PostRequestSync(string url, WWWForm data)
    {
        using (UnityWebRequest www = UnityWebRequest.Post(url, data))
        {
            var request = www.Send();

            while (!request.isDone) Thread.Sleep(5);

            if (www.isError)
            {
                Debug.Log("POST Error: " + url);
                return null;
            }
            else
            {
                Debug.Log("POST Succesful: " + url);
                return www.downloadHandler.text;
            }
        }
    }

}
